import React, { Component } from "react"
import BoxCon from "./container/box-container.js"
import "./index.css"
import { Route, HashRouter } from "react-router-dom"

import Home from "./content/Home"
import Stuff from "./content/Stuff"

class App extends Component {
  render() {
    return (
      <HashRouter>
        <div>
          <div className="content">
            <Route path="/" component={Home} />
          </div>
        </div>
      </HashRouter>
    )
  }
}

export default App

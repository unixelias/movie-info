let defaultState = {
  movieList: []
}

const mainReducer = (state = defaultState, action) => {
  switch (action.type) {
    case "CLICK_SEARCH":
      return {
        ...state,
        movieList: action.movieList
      }
    case "CHANGE_COLOR":
      return {
        ...state,
        color: action.color
      }
    default:
      return state
  }
}

export default mainReducer

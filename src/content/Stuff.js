import React, { Component } from "react"
import { NavLink } from "react-router-dom"

class Stuff extends Component {
  render() {
    return (
      <div>
        <h1>Buscador de Filmes</h1>
        <ul className="header">
          <li>
            <NavLink exact to="/">
              Filme
            </NavLink>
          </li>
          <li>
            <NavLink to="/stuff">Stuff</NavLink>
          </li>
        </ul>
        <h2>STUFF</h2>
        <p>
          Mauris sem velit, vehicula eget sodales vitae, rhoncus eget sapien:
        </p>
        <ol>
          <li>Nulla pulvinar diam</li>
          <li>Facilisis bibendum</li>
          <li>Vestibulum vulputate</li>
          <li>Eget erat</li>
          <li>Id porttitor</li>
        </ol>
      </div>
    )
  }
}

export default Stuff

import React from "react"
import { connect } from "react-redux"
import * as actionCreators from "../actions/index.js"
import SearchCon from "../container/search-container.js"

class Home extends React.Component {
  state = {
    searchKey: ""
  }
  render() {
    return (
      <div>
        <SearchCon
          makeSearch={this.props.loadSearch}
          movieList={this.props.movieList}
        />
      </div>
    )
  }
}

const mapStateToProps = state => {
  return state
}

export default connect(
  mapStateToProps,
  actionCreators
)(Home)

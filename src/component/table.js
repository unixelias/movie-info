import React, { Component } from "react"
import PropTypes from "prop-types"
import { Table } from "react-materialize"
import { connect } from "react-redux"
import * as actionCreators from "../actions/index.js"
import { __values } from "tslib"

class MovieTable extends Component {
  static propTypes = {
    data: PropTypes.array.isRequired
  }

  constructor(props) {
    super(props)
    this.state = {
      data: []
    }
  }

  static getDerivedStateFromProps(props, state) {
    // Any time the current user changes,
    // Reset any parts of state that are tied to that user.
    // In this simple example, that's just the email.
    if (props.movieList !== state.prevPropsMovieList) {
      return {
        data: [__values(props.movieList)]
      }
    }
    return null
  }

  renderProducts() {
    let products = [__values(this.state.data)]
    return products.map(function(product) {
      return (
        <tr key={product.imdbID}>
          <td>
            <div class="col s2">
              <img src={product.Poster} alt="" class="responsive-img poster" />
            </div>
          </td>
          <td>{product.Title}</td>
          <td>{product.Year}</td>
        </tr>
      )
    })
  }

  render() {
    return (
      <div className="container">
        <br />
        <Table className="table">
          <thead>
            <tr>
              <th>Poster</th>
              <th>Nome</th>
              <th>Ano</th>
            </tr>
          </thead>

          <tbody>{this.renderProducts()}</tbody>
        </Table>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return state
}

export default connect(
  mapStateToProps,
  actionCreators
)(MovieTable)

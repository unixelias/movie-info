import React, { Component } from "react"

class MovieTable extends Component {
  constructor(props) {
    super(props)
    this.state = {
      products: []
    }
  }
  componentDidMount() {
    if (this.props.data === undefined) {
      console.log("teste")
    } else {
      this.setState({ products: this.props.data })
    }
  }

  renderProducts() {
    return this.state.products.map(function(product) {
      return (
        <tr key={product.id}>
          <td>
            <div class="col s2">
              <img src={product.Poster} alt="" class="responsive-img poster" />
            </div>
          </td>
          <td>{product.Title}</td>
          <td>{product.Year}</td>
        </tr>
      )
    })
  }

  render() {
    return (
      <div className="container">
        <br />
        <table className="table">
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Email</th>
            </tr>
          </thead>

          <tbody>{this.renderProducts()}</tbody>
        </table>
      </div>
    )
  }
}

export default MovieTable

import React from "react"

class Box extends React.Component {
  render() {
    return (
      <div className="wrapper">
        <div className="box">
          <button
            onClick={() => {
              this.props.makeSearch()
            }}
          >
            Change Color
          </button>
        </div>
        <pre>{this.props.movieList}</pre>
      </div>
    )
  }
}

export default Box

import React from "react"
import { connect } from "react-redux"
import * as actionCreators from "../actions/index.js"
import Box from "../component/box.js"
import MovieTable from "../component/table.js"
import { Navbar, Table } from "react-materialize"
import axios from "axios"

class SearchCon extends React.Component {
  state = {
    searchKey: "",
    movieList: ""
  }
  doSearch = event => {
    this.setState({
      searchKey: event.target.value
    })
  }
  doSearch = event => {
    this.setState({
      searchKey: event.target.value
    })
  }
  handleClick() {
    return dispatch => {
      return axios
        .get("http://www.omdbapi.com/?apikey=68b7a486&s=Batman")
        .then(response => {
          dispatch(loadData(response.data.Search))
        })
    }
  }
  render() {
    const { loadSearch, movieList } = this.props
    const { searchKey } = this.state
    return (
      <div>
        <Box makeSearch={this.props.loadColor} color={this.props.color} />

        <div className="App">
          <Navbar brand="Home" center="true" />
          <div className="container">
            <div>
              <form action="#">
                <div className="input-field row">
                  <div className="input-field col s10">
                    <input
                      onChange={this.doSearch}
                      type="text"
                      id="title_search"
                      value={searchKey}
                    />
                    <label for="title_search">Nome do Filme</label>
                  </div>
                  <div
                    onClick={() => loadSearch(searchKey)}
                    className="btn input-field col s2"
                  >
                    <span>Buscar</span>
                  </div>
                </div>
              </form>
              <p>{searchKey}</p>
            </div>
            <div>
              <div className="button__container">
                <button className="button" onClick={this.handleClick}>
                  Click Me
                </button>
                <p>{this.state.movieList}</p>
              </div>
            </div>
            <MovieTable
              makeSearch={this.props.loadData}
              data={this.props.movieList}
            />
          </div>
        </div>
      </div>
    )
  }
}
const datasa = [
  { a: "123" },
  { a: "cdd", b: "edd" },
  { a: "1333", c: "eee", d: 2 }
]
console.log(datasa)

const mapStateToProps = state => {
  return state
}

export default connect(
  mapStateToProps,
  actionCreators
)(SearchCon)

import React from "react"
import { connect } from "react-redux"
import * as actionCreators from "../actions/index.js"
import Box from "../component/box.js"
import MovieTable from "../component/table.js"

class BoxCon extends React.Component {
  render() {
    return (
      <div>
        <Box makeSearch={this.props.loadColor} color={this.props.color} />
        <MovieTable data={data} />
      </div>
    )
  }
}

const data = [
  { a: "123" },
  { a: "cdd", b: "edd" },
  { a: "1333", c: "eee", d: 2 }
]

const mapStateToProps = state => {
  return state
}

export default connect(
  mapStateToProps,
  actionCreators
)(BoxCon)

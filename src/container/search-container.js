import React from "react"
import { connect } from "react-redux"
import * as actionCreators from "../actions/index.js"
import Box from "../component/box.js"
import MovieTable from "../component/table.js"

class SearchCon extends React.Component {
  super(props) {}
  state = {
    data: this.props.movieList
  }

  render() {
    return (
      <div>
        <Box
          makeSearch={this.props.loadSearch}
          movieList={this.props.movieList}
        />
        <MovieTable data={this.props.movieList} />
      </div>
    )
  }
}

const mapStateToProps = state => {
  return state
}

export default connect(
  mapStateToProps,
  actionCreators
)(SearchCon)

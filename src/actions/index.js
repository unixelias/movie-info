import axios from "axios"

export function loadSearch() {
  return dispatch => {
    return axios
      .get("http://www.omdbapi.com/?apikey=68b7a486&s=Batman")
      .then(response => {
        dispatch(loadData(response.data.Search))
        console.log(response.data.Search)
      })
  }
}

export function loadData(movieList) {
  return {
    type: "CLICK_SEARCH",
    movieList: JSON.stringify(movieList)
  }
}
